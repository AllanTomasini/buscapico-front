import {Component, ViewChild} from "@angular/core";
import {SplashScreen} from "@ionic-native/splash-screen";
import {StatusBar} from "@ionic-native/status-bar";
import {MenuController, Nav, Platform} from "ionic-angular";
import {AuthModel} from "../models/AuthModel";
import {LoginProvider} from "../providers/login/login.provider";

import {AuthProvider} from "../providers/auth/auth.provider";
import {AuthService} from "../providers/auth/auth.service";

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  @ViewChild(Nav)
  protected nav: Nav;
  protected rootPage: any;


  constructor(
    platform: Platform,
    public splashScreen: SplashScreen,
    public menuCtrl: MenuController)
  {
    platform.ready().then(() => {
      menuCtrl.enable(false);
      this.rootPage = "LoginPage";
      splashScreen.hide();
    });
  }




  private hideSplashScreen(): void {
    if (this.splashScreen) {
      setTimeout(() => {
        this.splashScreen.hide();
      }, 500);
    }
  }
}
