export interface Pico {
  nome: string;
  descricao: string;
  entrada: number;
  dataCriacao: Date;
}
