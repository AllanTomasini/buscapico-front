export class AuthModel {
  login: string;
  logged: boolean;
  jwt: string;
}
