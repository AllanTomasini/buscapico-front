export  interface Anuncio {
  titulo : string;
  preco : number;
  descricao : string;
  dataCadastro : Date;
}
