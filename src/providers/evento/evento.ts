import { Injectable } from '@angular/core';
import {HttpConfigProvider} from "../http-config/http-config";
import {Observable} from "rxjs";
import {Evento} from "../../models/evento";
import {API_CONFIG} from "../../config/api.config";
import {HttpClient} from "@angular/common/http";
import {Sessao} from "../../models/sessao";

/*
  Generated class for the EventoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
const path = "eventos";
@Injectable()
export class EventoProvider {
  constructor(public http: HttpClient) {
    console.log('Hello SessaoProvider Provider');
  }

  public adicionar(body) {
    return this.http.post(`${API_CONFIG.baseUrl}/${path}/inserir`, body, {
      observe: 'response',
      responseType: 'text'
    });
  }

  public listar(): Observable<Evento[]> {
    return this.http.get<Evento[]>(`${API_CONFIG.baseUrl}/${path}/listar`);
  }

  public findById(id: string) : Observable<Evento>{
    return this.http.get<Evento>(`${API_CONFIG.baseUrl}/${path}/${id}`);
  }

  deleteById(id: string):Observable<Evento> {
    return this.http.delete<Evento>(`${API_CONFIG.baseUrl}/${path}/${id}/excluir`);
  }

  editar(body , id: string) :Observable<Evento>{
    return this.http.put<Evento>(`${API_CONFIG.baseUrl}/${path}/${id}/editar`, body);
  }
}
