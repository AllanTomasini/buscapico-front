import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {HttpConfigProvider} from "../http-config/http-config";
import {Observable} from "rxjs";
import {Favorito} from "../../models/favorito";
import {API_CONFIG} from "../../config/api.config";

/*
  Generated class for the FavoritoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
const path = "favoritoes";
@Injectable()
export class FavoritoProvider {
  constructor(public http: HttpClient) {
  }
  public adicionar(body): Observable<any> {
    return this.http.post(`${API_CONFIG.baseUrl}/${path}/inserir`, body);
  }

  public listar(): Observable<Favorito[]> {
    return this.http.get<Favorito[]>(`${API_CONFIG.baseUrl}/${path}/listar`);
  }


}
