import {Injectable, Input, ViewChild} from "@angular/core";
import {HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs/Rx";
import {StorageService} from "../providers/storage/storage.service";
import {AlertController, Nav} from "ionic-angular";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  @Input()
  protected nav: Nav;
  constructor(public storage : StorageService, public alertCtrl : AlertController){}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .catch((error, caught) => {
        let errorObj = error;
        if (errorObj.error) {
          errorObj = errorObj.error;
        }
        if (!errorObj.status) {
          errorObj = JSON.parse(errorObj);
        }
        console.log("error detectado pelo interceptor: ");
        console.log(errorObj);

        switch (errorObj.status) {
          case 401:
            this.handle401();
            break;
          case 403:
            this.handle403();
            break;
          default:
            this.handleDefaultError(errorObj);
            break;
        }
        return Observable.throw(errorObj);
      }) as any;
  }
  handle403(){
    this.storage.setLocalUser(null);
    this.nav.setRoot("LoginPage");

  }
  handle401(){
    let alert = this.alertCtrl.create({
      title: 'Erro 401: falha de autenticação',
      message: 'Email ou senha incorretos',
      enableBackdropDismiss: false,
      buttons: [
        {text: 'Ok'}
      ]
    });
    alert.present();
  }

  handleDefaultError(errorObj){
    let alert = this.alertCtrl.create({
      title: 'Erro ' + errorObj.staus + ': ' + errorObj.error,
      message: errorObj.message,
      enableBackdropDismiss: false,
      buttons: [
        {text:'Ok'}
      ]
    });
    alert.present();
  }
}
export const ErrorInterceptorProvider = {
  provide : HTTP_INTERCEPTORS,
  useClass: ErrorInterceptor,
  multi: true,
}
