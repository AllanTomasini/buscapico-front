import { Component, Input } from "@angular/core";
import { Nav, MenuController } from "ionic-angular";
import {AuthService} from "../../providers/auth/auth.service";

/**
 * Generated class for the SideMenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

@Component({
  selector: "side-menu",
  templateUrl: "side-menu.html"
})
export class SideMenuComponent {
  @Input()
  protected nav: Nav;

  text: string;

  constructor(
    private menuCtrl: MenuController,
    private authService: AuthService
  ) {}

  onLoad(page: string) {
    this.nav.setRoot(page);
    this.menuCtrl.close();
  }

  logout() {
    this.authService.logout();
    this.menuCtrl.close();
    this.nav.setRoot("LoginPage");
  }
}
