import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AnuncioProvider} from "../../../../providers/anuncio/anuncio";


@IonicPage()
@Component({
  selector: 'page-cadastrar-anuncio',
  templateUrl: 'cadastrar-anuncio.html',
})
export class CadastrarAnuncioPage {

  private formAnuncio: FormGroup;
  private dataCriacao = new Date().toISOString();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public anuncioProvider: AnuncioProvider,
    public alertCtrl : AlertController
  ) {
  }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.formAnuncio = this.formBuilder.group({
      titulo: ["", Validators.required],
      preco: ["123.00", Validators.required],
      descricao: ["", Validators.required],
      dataCadastro: [],

    });

  }

  adicionarAnuncio() {
    let formValues = this.formAnuncio.getRawValue();
    console.log(formValues);

    let {titulo, preco, descricao, dataCadastro} = formValues;
    let body = {
      titulo,
      preco,
      descricao,
      dataCadastro

    };
    body.dataCadastro = this.dataCriacao;


    this.anuncioProvider.adicionar(body).subscribe(response => {
      this.cadastroSucesso();
      },
      error => {

      }
    );
  }

  cadastroSucesso() {
    let alert = this.alertCtrl.create({
      title: 'Sucesso!',
      message: 'Anúncio cadastrado com sucesso!',
      enableBackdropDismiss: false,
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.navCtrl.pop();
        }
      }
      ]
    });
    alert.present();
  }
}
