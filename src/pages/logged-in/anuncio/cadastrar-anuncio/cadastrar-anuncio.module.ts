import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastrarAnuncioPage } from './cadastrar-anuncio';

@NgModule({
  declarations: [
    CadastrarAnuncioPage,

  ],
  imports: [
    IonicPageModule.forChild(CadastrarAnuncioPage),

  ],
})
export class CadastrarAnuncioPageModule {}
