import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {StorageService} from "../../../../providers/storage/storage.service";
import {Usuario} from "../../../../models/usuario";
import {UsuarioProvider} from "../../../../providers/usuario/usuario";

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
usuario : Usuario;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage : StorageService,
              public usuarioProvider : UsuarioProvider) {
  }

  ionViewDidLoad() {
   let localUser = this.storage.getLocalUser();
    if (localUser && localUser.email){
      this.usuarioProvider.findByEmail(localUser.email).subscribe(response =>{
        this.usuario = response;
        }, error =>{
        if (error.status == 403){
          this.navCtrl.setRoot('LoginPage');
        }
      })
    }
    else{
      this.navCtrl.setRoot('LoginPage');
    }
  }

}
