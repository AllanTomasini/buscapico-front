import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListarSkatistasEtapasPage } from './listar-skatistas-etapas';

@NgModule({
  declarations: [
    ListarSkatistasEtapasPage,
  ],
  imports: [
    IonicPageModule.forChild(ListarSkatistasEtapasPage),
  ],
})
export class ListarSkatistasEtapasPageModule {}
