import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Anuncio} from "../../../../models/anuncio";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AnuncioProvider} from "../../../../providers/anuncio/anuncio";
import {Evento} from "../../../../models/evento";
import {EventoProvider} from "../../../../providers/evento/evento";

/**
 * Generated class for the EditarEventoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editar-evento',
  templateUrl: 'editar-evento.html',
})
export class EditarEventoPage {

  item: Evento = this.navParams.get('evento');
  private formEvento: FormGroup;
  item2 : Evento;
  id : string = this.navParams.get('id');
  private dataCriacao = new Date().toISOString();



  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public eventoProvider : EventoProvider
  ) {
  }
  ionViewDidLoad() {

  }
  ngOnInit() {
    this.initForm();
  }


  private initForm() {
    this.item2   = this.navParams.get('evento');
    this.formEvento = this.formBuilder.group({
      nome: [this.item2.nome, Validators.required]
    });


  }




  salvarSessao() {
    let formValues = this.formEvento.getRawValue();


    let {nome} = formValues;
    let body = {
      nome
    };

    this.eventoProvider.editar(body, this.id).subscribe(response => {
        this.navCtrl.pop();
      },
      error => {}
    );
  }

}
