import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {EventoProvider} from "../../../../providers/evento/evento";
import {Evento} from "../../../../models/evento";



@IonicPage()
@Component({
  selector: 'page-detalhes-evento',
  templateUrl: 'detalhes-evento.html',
})
export class DetalhesEventoPage {

  item: Evento;
  id: string = this.navParams.get('id');
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public eventoProvider: EventoProvider,
    public alertCtrl: AlertController) {
  }


  ionViewDidEnter() {
    this.carregarDetalhes();
  }

  ionViewDidLoad() {
   this.carregarDetalhes();
  }

  editarEvento(evento : Evento, id: string) {
    this.navCtrl.push("EditarEventoPage", {anuncio : evento, id : id});

  }

  excluirEvento() {
    let alert = this.alertCtrl.create({
      title: 'Confirmar exclusão do Evento',
      message: 'Você realmente deseja excluir esse Evento?',
      buttons: [
        {
          text: 'Não',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.eventoProvider.deleteById(this.id).subscribe(response => {
                this.item = response;
                this.navCtrl.pop();
              },
              error => {});
          }
        }
      ]
    })
    alert.present();
  }

  private carregarDetalhes() {
    let id = this.navParams.get('id');
    this.eventoProvider.findById(id)
      .subscribe(response => {
          this.item = response;
        },
        error => {});
  }
}
