import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {EventoProvider} from "../../../../providers/evento/evento";
import {Evento} from "../../../../models/evento";


@IonicPage()
@Component({
  selector: 'page-listar-eventos',
  templateUrl: 'listar-eventos.html',
})
export class ListarEventosPage {
  items : Evento [] ;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public loadingCtrl : LoadingController,
              public eventoProvider : EventoProvider) {
  }

  ionViewDidLoad() {
    this.loadData();
  }
  ionViewDidEnter(){
    this.loadData();
  }

  loadData() {

    let loader = this.presentLoading();
    this.eventoProvider.listar()
      .subscribe(response => {
          this.items = response;
          loader.dismiss();
          console.log(response);

        },
        error => {
          loader.dismiss();
        });
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    return loader;
  }

  cadastrarEvento() {
    this.navCtrl.push("CadastrarEventosPage");
  }

  detalhesEvento(id: string) {
    this.navCtrl.push("DetalhesEventoPage",{id : id});
  }
}
