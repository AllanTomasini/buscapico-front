import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComentarioSessaoPage } from './comentario-sessao';

@NgModule({
  declarations: [
    ComentarioSessaoPage,
  ],
  imports: [
    IonicPageModule.forChild(ComentarioSessaoPage),
  ],
})
export class ComentarioSessaoPageModule {}
