import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalhesSessaoPage } from './detalhes-sessao';

@NgModule({
  declarations: [
    DetalhesSessaoPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalhesSessaoPage),
  ],
})
export class DetalhesSessaoPageModule {}
