import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {SessaoProvider} from "../../../../providers/sessao/sessao";
import {Sessao} from "../../../../models/sessao";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

/**
 * Generated class for the EditarSessaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editar-sessao',
  templateUrl: 'editar-sessao.html',
})
export class EditarSessaoPage {
  item: Sessao = this.navParams.get('sessao');
  private dataCriacao = new Date().toISOString();
  private formSessao: FormGroup;
  id : string = this.navParams.get('id');



  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public sessaoProvider : SessaoProvider
  ) {
  }
  ionViewDidLoad() {

  }
  ngOnInit() {
    this.initForm();
  }


  private initForm() {


    this.formSessao = this.formBuilder.group({

      dataSessao : [  ,Validators.required],
      dataCadastro: [],
    });


  }




  salvarSessao() {
    let formValues = this.formSessao.getRawValue();
    console.log(formValues);

    let { dataSessao, dataCadastro} = formValues;
    let body = {

      dataSessao,
      dataCadastro
    };
    body.dataCadastro = this.item.dataCadastro;

    this.sessaoProvider.editar(body, this.id).subscribe(response => {
        console.log(response)
        this.navCtrl.pop();
      },
      error => {console.log(error)}
    );
  }
}
