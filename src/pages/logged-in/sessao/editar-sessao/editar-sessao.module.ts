import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditarSessaoPage } from './editar-sessao';

@NgModule({
  declarations: [
    EditarSessaoPage,
  ],
  imports: [
    IonicPageModule.forChild(EditarSessaoPage),
  ],
})
export class EditarSessaoPageModule {}
