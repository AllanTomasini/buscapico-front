import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Sessao} from "../../../../models/sessao";
import {SessaoProvider} from "../../../../providers/sessao/sessao";
import {FavoritoProvider} from "../../../../providers/favorito/favorito";
import {Favorito} from "../../../../models/favorito";

/**
 * Generated class for the PicoFavoritosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pico-favoritos',
  templateUrl: 'pico-favoritos.html',
})
export class PicoFavoritosPage {
  items : Favorito [] ;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public loadingCtrl : LoadingController,
              public favoritoProvider : FavoritoProvider) {
  }

  ionViewDidLoad() {
    this.loadData();
  }

  loadData() {

    let loader = this.presentLoading();
    this.favoritoProvider.listar()
      .subscribe(response => {
          this.items = response;
          loader.dismiss();
          console.log(response);

        },
        error => {
          loader.dismiss();
        });
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    return loader;
  }



}
