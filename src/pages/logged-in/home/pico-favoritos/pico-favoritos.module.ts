import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PicoFavoritosPage } from './pico-favoritos';

@NgModule({
  declarations: [
    PicoFavoritosPage,
  ],
  imports: [
    IonicPageModule.forChild(PicoFavoritosPage),
  ],
})
export class PicoFavoritosPageModule {}
